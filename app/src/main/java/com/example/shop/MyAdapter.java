package com.example.shop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MyAdapter extends ArrayAdapter<String> {
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View rowView = inflater.inflate(R.layout.item_user, parent, false);

        TextView textView = rowView.findViewById(R.id.textView);
        textView.setText(getItem(position));

        return rowView;
    }

    public MyAdapter(@NonNull Context context, @NonNull String[] objects) {
        super(context, R.layout.item_user, objects);



    }
}
