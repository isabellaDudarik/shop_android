package com.example.shop;

public class Generate {
    public static Information[] generate() {
        Information informations[] = new Information[6];
        informations[0] = new Information("Harry Potter", "1997", "Bloomsbury", "Joanne Rowling");
        informations[1] = new Information("Женщина в белом", "1860", "December", "Уилки Коллинз");
        informations[2] = new Information("Милый друг", "1885", "Kinsaung", "Ги де Мопассан");
        informations[3] = new Information("Брида", "1990", "Kinsaung_1", "Пауло Коэльо");
        informations[4] = new Information("Цветок орхидеи", "1948", "Bloomsbury", "Джеймс Хедли Чейз");
        informations[5] = new Information("Королёк — птичка певчая", "1922", "December", "Решат Нури Гюнтекин");
    return informations;
    }
}
