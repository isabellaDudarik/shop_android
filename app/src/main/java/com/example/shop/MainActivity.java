package com.example.shop;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Information[] informations = Generate.generate();

        final String users[] = {
                informations[0].name,
                informations[1].name,
                informations[2].name,
                informations[3].name,
                informations[4].name,
                informations[5].name
        };

        ListView listView = findViewById(R.id.list_view);
        MyAdapter adapter = new MyAdapter(this, users);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, String.format("publish year: %s,\npublish House: %s,\nauthor: %s",informations[position].publishYear, informations[position].publishHouse, informations[position].author), Toast.LENGTH_LONG).show();
            }
        });
    }
}
