package com.example.shop;

public class Information {
    String name;
    String publishYear;
    String publishHouse;
    String author;

    public Information(String name, String publishYear, String publishHouse, String author) {
        this.name = name;
        this.publishYear = publishYear;
        this.publishHouse = publishHouse;
        this.author = author;
    }

}
